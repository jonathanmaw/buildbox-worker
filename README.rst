What is ``buildbox-worker``?
============================

``buildbox-worker`` is a program that accepts jobs from a build server, invokes
a ``buildbox-run`` command to run them, and sends the results back. It
implements Bazel's `Remote Workers API`_.

It is designed to work with `BuildGrid`_ but does not depend on it.

.. _Remote Workers API: https://docs.google.com/document/d/1s_AzRRD2mdyktKUj2HWBn99rMg_3tcPvdjx3MPbFidU
.. _BuildGrid: https://buildgrid.build


Usage
=====

  buildbox-worker [OPTIONS] [BOT_ID]

This starts a worker with the given bot ID. (If no bot ID is specified, a
default one is generated using the machine's hostname and ``buildbox-worker``'s
process ID.)

Options:

* --concurrent-jobs=JOBS

  Number of jobs to run at once. If this isn't specified, jobs will be run one
  at a time.

* --stop-after=JOBS

  Terminate after running the given number of jobs. If this isn't specified,
  it's unlimited.

* --instance=INSTANCE

  Instance name to pass to the Remote Workers API and the CAS servers.
  Without this option, ``buildbox-worker`` will pass empty strings as instance
  names.

* --platform KEY VALUE

  Add a key-value pair to the "Platform" message the worker sends to the
  server. See the Remote Workers API specification for keys and values you can
  use.

* --buildbox-run=BUILDBOXRUN

  Change the command the worker uses to run jobs from ``buildbox-run`` to
  something else. The given command must support the same interface as
  ``buildbox-run`` (see below)

* --runner-arg=ARG

  Pass ``ARG`` to ``buildbox-run`` when the worker runs a job. This can be
  useful if the ``buildbox-run`` implementation you're using supports
  non-standard options.

* --bots-remote=URL (required)

  The URL for the RWAPI server.

* --bots-instance=NAME (only if `--instance` not set)

  Instance name of the RWAPI server.

  If `--instance` is given, uses its value.

* --bots-server-cert=PATH

  Path to the PEM-encoded public server certificate for https connections to
  the RWAPI server. This allows use of self-signed certificates.

* --bots-client-key=PATH

  Path to the PEM-encoded private client key for https with
  certificate-based client authentication. If this is specified,
  ``--bots-client-cert`` must be specified as well.

* --bots-client-cert=PATH

  Path to the PEM-encoded public client certificate for https with
  certificate-based client authentication. If this is specified,
  ``--bots-client-key`` must be specified as well.

* --cas-remote=URL (required)

  The URL for the Content-Addressable Storage server that job-related files
  will be fetched from and uploaded to.

* --cas-instance=NAME (only if `--instance` not set)

  Instance name of the CAS server.

  If `--instance` is given, uses its value.

* --cas-server-cert=PATH

  Path to the PEM-encoded public server certificate for https connections to
  the CAS server. This allows use of self-signed certificates.

* --cas-client-key=PATH

  Path to the PEM-encoded private client key for https with
  certificate-based client authentication. If this is specified,
  ``--cas-client-cert`` must be specified as well.

* --cas-client-cert=PATH

  Path to the PEM-encoded public client certificate for https with
  certificate-based client authentication. If this is specified,
  ``--cas-client-key`` must be specified as well.

* --metrics-mode=MODE

  Where mode is one of the following: ``udp://<hostname>:<port>``, ``file:///path/to/file``, or ``stderr``

* --metrics-publish-interval=VALUE

  Where VALUE is the number of seconds that the background thread sleeps in between publishing metrics

* --verbose

  Output additional information to stdout that may be useful when debugging.

* --config-file=FILE

  Optionally, read in FILE to override default startup configuration. See buildbox-worker configuration file section for more information on the format of this file.


::

    usage: ./buildbox-worker [OPTIONS] [BOT_ID]
        --concurrent-jobs=JOBS             Number of jobs to run concurrently
        --stop-after=JOBS                  Stop after running this many jobs
        --instance=INSTANCE                Set the instance name of both CAS and Bots services
        --platform KEY VALUE               Set a platform property
        --request-timeout=TIME(seconds)    Sets the timeout for updateBotSession request. Defaults to 1s.
        --buildbox-run=BUILDBOXRUN         Path to buildbox-run binary
        --runner-arg=ARG                   Pass ARG to buildbox-run
        --bots-remote=URL                  URL for Bots service
        --bots-instance=NAME               Name of the Bots instance
        --bots-server-cert=PATH            Public server certificate for TLS (PEM-encoded)
        --bots-client-key=PATH             Private client key for TLS (PEM-encoded)
        --bots-client-cert=PATH            Public client certificate for TLS (PEM-encoded)
        --bots-retry-limit=INT             Number of times to retry on grpc errors
        --bots-retry-delay=SECONDS         How long to wait between grpc retries
        --cas-remote=URL                   URL for CAS service
        --cas-instance=NAME                Name of the CAS instance
        --cas-server-cert=PATH             Public server certificate for TLS (PEM-encoded)
        --cas-client-key=PATH              Private client key for TLS (PEM-encoded)
        --cas-client-cert=PATH             Public client certificate for TLS (PEM-encoded)
        --cas-retry-limit=INT              Number of times to retry on grpc errors
        --cas-retry-delay=SECONDS          How long to wait between grpc retries
        --log-level=LEVEL                  (default: info) Log verbosity: trace/debug/info/warning/error
        --metrics-mode=MODE                Options for MODE are:
             udp://<hostname>:<port>
             file:///path/to/file
             stderr
                                           Only one metric output mode can be specified
        --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds
        --verbose                          Set log level to debug
        --log-file=FILE                    File to write log to
        --config-file=FILE                 Configuration file


The ``buildbox-run`` interface
==============================

``buildbox-worker`` invokes a ``buildbox-run`` command to download a job's
files, run the job, and upload the results. You can change the command with
the ``--buildbox-run=BUILDBOXRUN`` option to change the fetching and
sandboxing mechanisms the worker uses.

The command you specify must support the following interface:

  BUILDBOXRUN --remote=URL [--instance=NAME] [--server-cert=CERT] [--server-key=KEY]
  [--client-key=KEY] --action=FILE --action-result=FILE

``--remote`` and  ``--instance`` (empty by default) specify the CAS server.
``--server-cert``, ``--server-key``, and ``--client-key`` work as they do in
``buildbox-worker``.  ``--action`` specifies the path to a file with a
serialized Action protocol buffer, and ``--action-result`` is the path to write
the corresponding ActionResult to.


The ``buildbox-worker configuration`` file
==============================

``buildbox-worker`` supports a protobuf-based configuration enabled via the
``--config-file=FILE`` parameter, where 'FILE' points to the absolute path of
the file. This file currently only supports a 'botStatus' entry. Format of the
file is:

botStatus: [0-4]

To be backwards compatible, if no ``--config-file=FILE`` is on the command-line,
the worker will set it's initial status to ``OK``.

Otherwise, the worker will read in the status from the config file and use that
as the initial state to be passed to a bots service.

Metrics
=======
buildbox-worker publishes runtime metrics throughout its lifetime. Documentation listing the names of these published metrics can be found in `buildboxworker_metricnames.cpp`_ and a brief description of them can be found in the `buildboxworker_metricnames.h`_ header file.

.. _buildboxworker_metricnames.cpp: https://gitlab.com/BuildGrid/buildbox/buildbox-worker/-/blob/master/buildboxworker/buildboxworker_metricnames.cpp
.. _buildboxworker_metricnames.h: https://gitlab.com/BuildGrid/buildbox/buildbox-worker/-/blob/master/buildboxworker/buildboxworker_metricnames.h

Metrics Enablement
------------------
To enable metrics publishing, two command line options are required:
::

   --metrics-mode=MODE                Options for MODE are:
           udp://<hostname>:<port>
           file:///path/to/file
           stderr

   --metrics-publish-interval=VALUE   Publish metric at the specified interval rate in seconds, defaults 15 seconds

Example #1: Write metrics to a statsd server on the local host listening on port 50051 and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=udp://localhost:50051 --metrics-publish-interval=5

Example #2: Write metrics to stderr and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=stderr --metrics-publish-interval=5

Example #3: Write metrics to a file and configure the background publishing thread to publish every 5 seconds
::
   --metrics-mode=file:///tmp/my-metrics.log --metrics-publish-interval=5
