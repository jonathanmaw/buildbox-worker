#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <buildboxworker_cmdlineparser.h>
#include <buildboxworker_worker.h>

using namespace buildboxworker;

using namespace testing;

void init(int &argc, char *args[], const bool oldPlatformFormat = true)
{
    args[argc++] = (char *)"--instance=dev";
    args[argc++] = (char *)"--cas-remote=http://127.0.0.1:50011";
    args[argc++] = (char *)"--bots-remote=http://100.70.37.178:50051";
    args[argc++] = (char *)"--log-level=debug";
    args[argc++] = (char *)"--request-timeout=3";
    args[argc++] =
        (char *)"--buildbox-run=/opt/remoteexecution/buildbox-run-hosttools";
    args[argc++] = (char *)"--runner-arg=--prefix-staged-dir";

    if (oldPlatformFormat) {
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"OSFamily";
        args[argc++] = (char *)"linux";
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"ISA";
        args[argc++] = (char *)"x86-64";
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"ISA";
        args[argc++] = (char *)"x86-32";
    }
    else {
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"OSFamily=linux";
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"ISA=x86-64";
        args[argc++] = (char *)"--platform";
        args[argc++] = (char *)"ISA=x86-32";
    }

    args[argc++] =
        (char *)"--config-file=/dbldroot/data/chroot/buildboxworker-1/opt/"
                "remoteexecution/buildboxworker.conf";
}

void tester(Worker &worker, const std::string &instanceName)
{
    ASSERT_STREQ("dev", instanceName.c_str());
    ASSERT_STREQ("http://127.0.0.1:50011", worker.d_casServer.d_url);
    ASSERT_STREQ("http://100.70.37.178:50051", worker.d_botsServer.d_url);
    ASSERT_STREQ("debug", worker.d_logLevel.c_str());
    ASSERT_EQ(3, worker.d_requestTimeout);
    ASSERT_EQ("/opt/remoteexecution/buildbox-run-hosttools",
              worker.d_runnerCommand);
    ASSERT_STREQ("--prefix-staged-dir", worker.d_extraRunArgs[0].c_str());
    ASSERT_THAT(worker.d_platform,
                ElementsAre(Pair("OSFamily", "linux"), Pair("ISA", "x86-64"),
                            Pair("ISA", "x86-32")));
    ASSERT_STREQ("/dbldroot/data/chroot/buildboxworker-1/opt/remoteexecution/"
                 "buildboxworker.conf",
                 worker.d_configFileName.c_str());
}

TEST(ParserTest, BasicTest)
{
    Worker worker;
    buildboxcommonmetrics::MetricsConfigType config;
    std::string instanceName;
    int argc = 0;
    char *args[32];

    init(argc, args);
    const bool rc = Parser::parse(&worker, &config, &instanceName, argc, args);
    EXPECT_TRUE(rc);
    tester(worker, instanceName);
}

TEST(ParserTest, NewPlatformTest)
{
    Worker worker;
    buildboxcommonmetrics::MetricsConfigType config;
    std::string instanceName;
    int argc = 0;
    char *args[32];

    init(argc, args, false);
    const bool rc = Parser::parse(&worker, &config, &instanceName, argc, args);
    EXPECT_TRUE(rc);
    tester(worker, instanceName);
}
