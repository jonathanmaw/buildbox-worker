#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <gmock/gmock.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>
#include <gtest/gtest.h>

#include <buildboxworker_actionutils.h>
#include <buildboxworker_metricnames.h>
#include <buildboxworker_worker.h>

#include <buildboxcommon_temporaryfile.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

using namespace testing;
using buildboxcommon::TemporaryFile;
using buildboxcommon::buildboxcommonmetrics::DurationMetricTimer;
using buildboxcommon::buildboxcommonmetrics::validateMetricCollection;
using buildboxworker::ActionUtils;

TEST(ActionUtilsTests, ReadActionResultFileMissing)
{
    const std::string actionFileName("this_file_does_not_exist.txt");
    ASSERT_FALSE(
        buildboxcommon::FileUtils::isRegularFile(actionFileName.c_str()));
    ASSERT_THROW(ActionUtils::readActionResultFile(actionFileName.c_str()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileInvalidFile)
{
    const std::string actionFileName("invalid_actionresult.txt");
    ASSERT_TRUE(
        buildboxcommon::FileUtils::isRegularFile(actionFileName.c_str()));
    EXPECT_THROW(ActionUtils::readActionResultFile(actionFileName.c_str()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileEmptyFile)
{
    buildboxcommon::TemporaryFile emptyActionResult;
    EXPECT_THROW(ActionUtils::readActionResultFile(emptyActionResult.name()),
                 std::runtime_error);
}

TEST(ActionUtilsTests, ReadActionResultFileNormal)
{
    const std::string actionFileName("sample_actionresult.txt");
    auto result = ActionUtils::readActionResultFile(actionFileName.c_str());
    ASSERT_EQ(result.exit_code(), 0);
}

TEST(ActionUtilsTests, ReadActionResultFileMetricsCollected)
{
    const std::string actionFileName("sample_actionresult.txt");
    ActionUtils::readActionResultFile(actionFileName.c_str());
    ASSERT_TRUE(validateMetricCollection<DurationMetricTimer>(
        buildboxworker::MetricNames::TIMER_NAME_READ_ACTION_RESULT));
}

class MockActionUtils : public ActionUtils {
    MOCK_METHOD1(runCommandInSubprocess,
                 pid_t(const std::vector<std::string> args));
    MOCK_METHOD2(waitForRunner,
                 void(pid_t subprocessPid, buildboxworker::Worker *worker));
};

TEST(ActionUtilsTests, ExecuteActionInSubprocessMetricsCollected)
{
    buildboxworker::Worker worker;
    MockActionUtils::executeActionInSubprocess({"ls"}, &worker);
    ASSERT_TRUE(validateMetricCollection<DurationMetricTimer>(
        buildboxworker::MetricNames::TIMER_NAME_EXECUTE_ACTION));
}

class DownloadActionTests : public ::testing::Test {
  protected:
    TemporaryFile testActionFile;
    proto::BotSession botSession;
    // std::string casServer = "https://foobar";
    buildboxcommon::ConnectionOptions casServerConnection;
    proto::Lease *lease;
    DownloadActionTests()
    {
        lease = botSession.add_leases();
        lease->set_id("42");
    }
};

TEST_F(DownloadActionTests, DownloadActionMetricsCollected)
{
    proto::Action action;
    google::protobuf::Any *payload = lease->mutable_payload();
    payload->PackFrom(action);
    ActionUtils::downloadActionToFile("42", &testActionFile,
                                      casServerConnection, botSession);

    ASSERT_TRUE(validateMetricCollection<DurationMetricTimer>(
        buildboxworker::MetricNames::TIMER_NAME_DOWNLOAD_ACTION));
}
