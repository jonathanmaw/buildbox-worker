#include <gtest/gtest.h>

#include <google/devtools/remoteworkers/v1test2/bots_mock.grpc.pb.h>

#include <buildboxworker_botsessionutils.h>

#include <buildboxcommon_protos.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_testingutils.h>
#include <buildboxworker_metricnames.h>

using namespace testing;
using namespace buildboxworker::proto;
using buildboxcommon::buildboxcommonmetrics::DurationMetricTimer;
using buildboxcommon::buildboxcommonmetrics::validateMetricCollection;
using buildboxworker::BotSessionUtils;

using google::devtools::remoteworkers::v1test2::MockBotsStub;

class BotSessionUtilsTests : public ::testing::Test {
  protected:
    BotStatus botStatus = BotStatus::OK;
    std::shared_ptr<Bots::StubInterface> stub;
    long requestTimeout = 1.0;
    BotSession botSession;
    buildboxcommon::ConnectionOptions botsServerConnection;
    BotSessionUtilsTests() { stub = std::make_shared<MockBotsStub>(); }
};

/*
 * UpdateBotSession succeeds on the first try.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionHappyPath)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, botSession,
                                      requestTimeout, botsServerConnection);
}

/*
 * UpdateBotSession succeeds after one retry.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionEventualSuccess)
{
    botsServerConnection.d_retryLimit = "1";
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(
            grpc::Status(grpc::FAILED_PRECONDITION, "Failing for test")))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, botSession,
                                      requestTimeout, botsServerConnection);
}

/*
 * Verify that metrics are collected for UpdateBotSession.
 */
TEST_F(BotSessionUtilsTests, UpdateBotSessionMetricsCollected)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), UpdateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::updateBotSession(botStatus, stub, botSession,
                                      requestTimeout, botsServerConnection);
    ASSERT_TRUE(validateMetricCollection<DurationMetricTimer>(
        buildboxworker::MetricNames::TIMER_NAME_UPDATE_BOTSESSION));
}

class CreateBotSessionTests : public BotSessionUtilsTests {
  protected:
    CreateBotSessionTests()
    {
        botSession.set_bot_id("foo");
        botsServerConnection.d_instanceName = "dev";
    }
};

TEST_F(CreateBotSessionTests, CreateBotSessionHappyPath)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), CreateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::createBotSession(stub, botSession, botsServerConnection);
}

TEST_F(CreateBotSessionTests, CreateBotSessionMetricsCollected)
{
    EXPECT_CALL(dynamic_cast<MockBotsStub &>(*stub), CreateBotSession(_, _, _))
        .WillOnce(Return(grpc::Status::OK));
    BotSessionUtils::createBotSession(stub, botSession, botsServerConnection);
    ASSERT_TRUE(validateMetricCollection<DurationMetricTimer>(
        buildboxworker::MetricNames::TIMER_NAME_CREATE_BOTSESSION));
}
