/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_CMDLINEPARSER
#define INCLUDED_BUILDBOXWORKER_CMDLINEPARSER

#include <buildboxcommonmetrics_metricsconfigurator.h>
#include <string>

using namespace buildboxcommon;

namespace buildboxworker {

class Worker;

struct Parser {
    /**
     * Parse command line arguments and populate Worker object with parsed data
     * Returns: true on success, false on a failure of any type
     */
    static bool parse(Worker *worker,
                      buildboxcommonmetrics::MetricsConfigType *metricConfig,
                      std::string *global_instance_name, int argc,
                      char *argv[]);
};

} // namespace buildboxworker

#endif
