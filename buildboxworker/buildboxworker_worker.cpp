/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_worker.h>

#include <buildboxworker_actionutils.h>
#include <buildboxworker_botsessionutils.h>
#include <buildboxworker_config.h>
#include <buildboxworker_expiretime.h>
#include <buildboxworker_metricnames.h>

#include <buildboxcommon_client.h>
#include <buildboxcommon_exception.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_systemutils.h>
#include <buildboxcommonmetrics_durationmetrictimer.h>
#include <buildboxcommonmetrics_metricguard.h>

#include <google/devtools/remoteworkers/v1test2/worker.grpc.pb.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>

#include <csignal>
#include <cstring>
#include <exception>
#include <fcntl.h>
#include <iomanip>
#include <pthread.h>
#include <signal.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <system_error>
#include <thread>
#include <unistd.h>

namespace buildboxworker {

const std::chrono::microseconds
    Worker::s_defaultWaitTime(std::chrono::milliseconds(250));

const std::chrono::microseconds
    Worker::s_maxWaitTime(std::chrono::minutes(10));

namespace {

/**
 * string-ifys a std::chrono::time_point to a human-readable time in
 * microsecond res
 */
std::string timePointToStr(const std::chrono::system_clock::time_point &tp)
{
    const time_t nowAsTimeT = std::chrono::system_clock::to_time_t(tp);
    const std::chrono::microseconds micros =
        std::chrono::duration_cast<std::chrono::microseconds>(
            tp.time_since_epoch()) %
        static_cast<int>(1e6);
    struct tm localtime;
    localtime_r(&nowAsTimeT, &localtime);

    std::ostringstream os;
    os << std::put_time(&localtime, "%FT%T") << '.' << std::setfill('0')
       << std::setw(3) << micros.count() << std::put_time(&localtime, "%z");

    return os.str();
}

/**
 * Convenience functions for determining which type of signal was just received
 */
bool rereadConfigRequested(const int signal) { return (signal == SIGHUP); }

bool shutdownRequested(const int signal)
{
    return (signal == SIGINT || signal == SIGTERM);
}

volatile std::sig_atomic_t k_signalStatus;

} // namespace

void signal_handler(int signal) { k_signalStatus = signal; }

std::vector<std::string>
Worker::buildRunnerCommand(TemporaryFile &actionFile,
                           TemporaryFile &actionResultFile)
{

    // `ActionUtils::executeActionInSubprocess()` needs the full path to the
    // runner binary.
    const auto path_to_runner =
        SystemUtils::getPathToCommand(this->d_runnerCommand);
    if (path_to_runner.empty()) {
        BUILDBOXCOMMON_THROW_EXCEPTION(std::runtime_error,
                                       "Could not find runner command \"" +
                                           this->d_runnerCommand + "\"");
    }

    std::vector<std::string> command = {path_to_runner};

    command.insert(command.end(), this->d_extraRunArgs.begin(),
                   this->d_extraRunArgs.end());
    this->d_casServer.putArgs(&command);

    command.push_back(std::string("--action=") + actionFile.name());
    command.push_back(std::string("--action-result=") +
                      actionResultFile.name());

    command.push_back("--log-level=" + this->d_logLevel);
    if (this->d_logFile != "") {
        command.push_back("--log-file=" + this->d_logFile);
    }

    return command;
}

proto::ActionResult Worker::executeAction(TemporaryFile &actionFile)
{
    TemporaryFile actionResultFile;
    actionFile.close();
    actionResultFile.close();

    std::vector<std::string> command =
        buildRunnerCommand(actionFile, actionResultFile);

    const pid_t subprocessPid =
        ActionUtils::executeActionInSubprocess(command, this);

    if (shutdownRequested(k_signalStatus)) {
        decrementThreadCount();
        pthread_exit(nullptr);
    }

    // Only erase processGid from the set if waitpid returned with no
    // errors.
    {
        std::lock_guard<std::mutex> lock(this->d_sessionMutex);
        d_subprocessPgid.erase(subprocessPid);
    }

    const proto::ActionResult actionResult =
        ActionUtils::readActionResultFile(actionResultFile.name());
    BUILDBOX_LOG_DEBUG("ActionResult " << actionResult.DebugString());

    return actionResult;
}

void Worker::workerThread(const std::string &leaseId)
{
    BUILDBOX_LOG_DEBUG("Starting worker thread for leaseId " << leaseId);
    proto::ActionResult result;

    if (shutdownRequested(k_signalStatus)) {
        decrementThreadCount();
        return;
    }

    google::rpc::Status leaseStatus;
    leaseStatus.set_code(google::rpc::Code::OK);
    try {
        TemporaryFile actionFile;
        BUILDBOX_LOG_DEBUG("Downloading payload to " << actionFile.name());

        { // Lock while downloading the action
            std::lock_guard<std::mutex> lock(this->d_sessionMutex);

            // Get the Action to execute from the session.
            const bool lease_found = ActionUtils::downloadActionToFile(
                leaseId, &actionFile, d_casServer, d_session);

            if (!lease_found) {
                --this->d_detachedThreadCount;
                this->d_sessionCondition.notify_all();
                return;
            }
        }

        result = executeAction(actionFile);
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_ERROR("Caught exception in worker: " << e.what());
        leaseStatus.set_code(google::rpc::Code::ABORTED);
        leaseStatus.set_message(
            "System error when attempting to execute Action: [" +
            std::string(e.what()) + "]");
    }

    {
        // Store the ActionResult back in the lease.
        //
        // We need to search for the lease again because it could have been
        // moved/cancelled/deleted/completed by someone else while we were
        // executing it.
        std::lock_guard<std::mutex> lock(this->d_sessionMutex);
        for (auto &lease : *this->d_session.mutable_leases()) {
            if (lease.id() == leaseId &&
                lease.state() == proto::LeaseState::ACTIVE) {
                lease.set_state(proto::LeaseState::COMPLETED);
                lease.mutable_status()->CopyFrom(leaseStatus);
                if (leaseStatus.code() == google::rpc::Code::OK) {
                    lease.mutable_result()->PackFrom(result);
                }
            }
        }
        this->d_activeJobs.erase(leaseId);
        --this->d_detachedThreadCount;
    }
    this->d_sessionCondition.notify_all();
}

void Worker::registerSignals()
{
    struct sigaction sa;
    sa.sa_handler = signal_handler;
    sigemptyset(&sa.sa_mask);
    sa.sa_flags = SA_SIGINFO;

    if (sigaction(SIGINT, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGINT");
        exit(1);
    }
    if (sigaction(SIGTERM, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGTERM");
        exit(1);
    }
    if (sigaction(SIGHUP, &sa, nullptr) == -1) {
        BUILDBOX_LOG_INFO("Unable to register signal handler for SIGHUP");
        exit(1);
    }
}

void Worker::setPlatformProperties()
{
    auto workerProto = this->d_session.mutable_worker();
    auto device = workerProto->add_devices();
    device->set_handle(this->d_botID);

    for (const auto &platformPair : this->d_platform) {
        // TODO Differentiate worker properties and device properties?
        auto workerProperty = workerProto->add_properties();
        workerProperty->set_key(platformPair.first);
        workerProperty->set_value(platformPair.second);

        auto deviceProperty = device->add_properties();
        deviceProperty->set_key(platformPair.first);
        deviceProperty->set_value(platformPair.second);
    }
}

bool Worker::hasJobsToProcess() const
{
    return (this->d_stopAfterJobs != 0 || this->d_activeJobs.size() > 0 ||
            this->d_jobsPendingAck.size() > 0);
}

void Worker::processLeases(bool *skipPollDelay)
{
    for (auto &lease : *this->d_session.mutable_leases()) {
        if (lease.state() == proto::LeaseState::PENDING) {
            processPendingLease(&lease, skipPollDelay);
        }
        else if (lease.state() == proto::LeaseState::ACTIVE) {
            processActiveLease(lease);
        }
        else if (lease.state() == proto::LeaseState::CANCELLED) {
            processCancelledLease(lease);
        }
    }
}

void Worker::processPendingLease(proto::Lease *lease, bool *skipPollDelay)
{
    BUILDBOX_LOG_DEBUG("Processing pending lease: " << lease->DebugString());
    if (lease->payload().Is<proto::Action>() ||
        lease->payload().Is<proto::Digest>()) {

        if (static_cast<int>(this->d_activeJobs.size() +
                             this->d_jobsPendingAck.size()) <
                this->d_maxConcurrentJobs &&
            this->d_stopAfterJobs != 0) {
            // Accept the lease, but wait for the server's ack
            // before actually starting work on it.
            lease->set_state(proto::LeaseState::ACTIVE);
            *skipPollDelay = true;
            this->d_jobsPendingAck.insert(lease->id());

            if (this->d_stopAfterJobs > 0) {
                this->d_stopAfterJobs--;
            }
        }
    }
    else {
        BUILDBOX_LOG_ERROR("Lease payload is of an unexpected type (not "
                           "`Action` or `Digest`): "
                           << lease->DebugString());

        auto status = lease->mutable_status();
        status->set_message("Invalid lease");
        status->set_code(google::rpc::Code::INVALID_ARGUMENT);

        lease->set_state(proto::LeaseState::COMPLETED);
    }
}

void Worker::processActiveLease(const proto::Lease &lease)
{
    BUILDBOX_LOG_DEBUG("Processing active lease: " << lease.DebugString());

    this->d_jobsPendingAck.erase(lease.id());

    if (this->d_activeJobs.count(lease.id()) == 0) {
        const std::string leaseId = lease.id();
        auto thread =
            std::thread([this, leaseId] { this->workerThread(leaseId); });
        ++this->d_detachedThreadCount;
        thread.detach();
        // (the thread will remove its job from activeJobs when
        // it's done)
        this->d_activeJobs.insert(lease.id());
    }
}

void Worker::processCancelledLease(const proto::Lease &lease)
{
    BUILDBOX_LOG_DEBUG("Processing cancelled lease: " << lease.DebugString());

    if (this->d_jobsPendingAck.count(lease.id()) != 0) {
        // We accepted the job, but the server decided that we
        // shouldn't run it after all.
        this->d_jobsPendingAck.erase(lease.id());
        if (this->d_stopAfterJobs >= 0) {
            this->d_stopAfterJobs++;
        }
    }
}

std::chrono::microseconds Worker::calculateWaitTime() const
{
    if (!this->d_session.has_expire_time() || this->d_activeJobs.size() == 0) {
        // `expire_time` may be way too long if we don't have any pending
        // work, and that would mean workers won't pick up work for a
        // while. So in that case we use the default value.
        return s_defaultWaitTime;
    }

    // convert google.protobuf.Timestamp to std::chrono::time_point
    const auto expireTime =
        ExpireTime::convertToTimePoint(this->d_session.expire_time());

    // get current time
    const auto now = std::chrono::system_clock::now();

    if (expireTime <= now) {
        BUILDBOX_LOG_WARNING(
            "BotSession::expire_time is either "
            "now or in the past: expireTime = "
            << timePointToStr(expireTime) << ", now = " << timePointToStr(now)
            << ", using default wait time of "
            << std::chrono::duration_cast<std::chrono::seconds>(
                   s_defaultWaitTime)
                   .count()
            << " seconds");

        return s_defaultWaitTime;
    }

    // calc the difference
    auto waitTime = std::chrono::duration_cast<std::chrono::microseconds>(
        expireTime - now);

    // minus some percentage of time allow us to respond
    const auto factor = static_cast<int64_t>(
        waitTime.count() * ExpireTime::updateTimeoutPaddingFactor());
    waitTime -= std::chrono::microseconds(factor);

    // check for sane upper bounds
    if (waitTime > s_maxWaitTime) {
        BUILDBOX_LOG_WARNING(
            "detected excessive BotSession::expire_time set to "
            << waitTime.count() << "us, using max wait time of "
            << std::chrono::duration_cast<std::chrono::seconds>(s_maxWaitTime)
                   .count()
            << " seconds");

        waitTime = s_maxWaitTime;
    }

    return waitTime;
}

void Worker::decrementThreadCount()
{
    std::lock_guard<std::mutex> lock(this->d_sessionMutex);
    --this->d_detachedThreadCount;
    this->d_sessionCondition.notify_all();
}

void Worker::cleanupAfterUpdateBotSession(grpc::Status updateStatus,
                                          bool shouldExit)
{
    if (!updateStatus.ok()) {
        std::string message = "Failed to update bot status: gRPC error " +
                              to_string(updateStatus.error_code()) + ": " +
                              updateStatus.error_message();
        // grpcRetry will have logged the error for us.
        if (shouldExit) {
            std::lock_guard<std::mutex> lock(d_sessionMutex);
            BUILDBOX_LOG_ERROR("Unable to update bot session. Exiting, and "
                               "killing: ["
                               << d_subprocessPgid.size()
                               << "] subprocesses.");
            for (const auto &gpid : d_subprocessPgid) {
                if (killpg(gpid, SIGTERM) != 0) {
                    BUILDBOX_LOG_INFO("Sending signal to group pid: ["
                                      << gpid << "].");
                    // Continue if no process group id is found
                    if (errno != ESRCH) {
                        BUILDBOX_LOG_ERROR("Unable to send SIGTERM to "
                                           "processes with group pid: ["
                                           << gpid << "]. Due to: "
                                           << strerror(errno));
                        exit(1);
                    }
                }
            }
        }
    }
}

void Worker::trackSubprocess(const pid_t subprocessPid)
{
    std::lock_guard<std::mutex> lock(this->d_sessionMutex);
    d_subprocessPgid.insert(subprocessPid);
}

void Worker::runWorker()
{
    registerSignals(); // Handle SIGINT, SIGTERM and SIGHUP

    BUILDBOX_LOG_INFO("Starting build worker with bot ID \"" << d_botID
                                                             << "\"");

    this->d_session.set_bot_id(this->d_botID);
    setPlatformProperties();

    this->d_stub =
        std::move(proto::Bots::NewStub(this->d_botsServer.createChannel()));

    const auto createSessionStatus =
        BotSessionUtils::createBotSession(d_stub, d_session, d_botsServer);
    if (!createSessionStatus.ok()) {
        BUILDBOX_LOG_ERROR("Failed to create bot session: gRPC error " +
                           to_string(createSessionStatus.error_code()) + ": " +
                           createSessionStatus.error_message());
        exit(1);
    }

    BUILDBOX_LOG_DEBUG("Bot Session created. Now waiting for jobs.");

    bool skipPollDelay = true;

    while (this->hasJobsToProcess() && !shutdownRequested(k_signalStatus)) {
        std::unique_lock<std::mutex> lock(this->d_sessionMutex);
        if (skipPollDelay) {
            skipPollDelay = false;
        }
        else {
            this->d_sessionCondition.wait_for(lock, this->calculateWaitTime());
        }

        if (rereadConfigRequested(k_signalStatus)) {
            d_botStatus =
                Config::getStatusFromConfigFile(this->d_configFileName);
            k_signalStatus = 0;
        }

        processLeases(&skipPollDelay);
        grpc::Status updateStatus = BotSessionUtils::updateBotSession(
            d_botStatus, d_stub, d_session, d_requestTimeout, d_botsServer);

        cleanupAfterUpdateBotSession(updateStatus);
    }

    if (shutdownRequested(k_signalStatus)) {
        BUILDBOX_LOG_DEBUG("Received signal, exiting.");
        std::unique_lock<std::mutex> lock(this->d_sessionMutex);
        // Wait for all detached threads to finish.
        while (this->d_detachedThreadCount > 0) {
            this->d_sessionCondition.wait(lock);
            // Don't exit immediately when signal is being handled.
            grpc::Status updateStatus = BotSessionUtils::updateBotSession(
                proto::BotStatus::BOT_TERMINATING, d_stub, d_session,
                d_requestTimeout, d_botsServer);

            cleanupAfterUpdateBotSession(updateStatus, false);
        }
        exit(1);
    }
}
} // namespace buildboxworker
