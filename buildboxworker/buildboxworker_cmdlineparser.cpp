/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcommon_logging.h>
#include <buildboxcommonmetrics_metricsconfigurator.h>
#include <buildboxworker_cmdlineparser.h>
#include <buildboxworker_worker.h>

namespace buildboxworker {

bool Parser::parse(Worker *worker,
                   buildboxcommonmetrics::MetricsConfigType *metricConfig,
                   std::string *global_instance_name, int argc, char *argv[])
{
    if (argc == 0 || argv == nullptr) {
        BUILDBOX_LOG_ERROR("invalid argc/argv parameters");
        return false;
    }

    while (argc > 0) {
        const char *arg = argv[0];
        const char *assign = strchr(arg, '=');
        if (worker->d_botsServer.parseArg(arg, "bots-") ||
            worker->d_casServer.parseArg(arg, "cas-")) {
            // Argument was handled by server_opts.
        }
        else if (arg[0] == '-' && arg[1] == '-') {
            arg += 2;
            if (assign) {
                const auto key_len = static_cast<size_t>(assign - arg);
                const char *value = assign + 1;

                const auto str_arg = std::string(arg, key_len);
                if (buildboxcommonmetrics::MetricsConfigurator::
                        isMetricsOption(str_arg)) {
                    buildboxcommonmetrics::MetricsConfigurator::metricsParser(
                        str_arg, value, metricConfig);
                    argv++;
                    argc--;
                    continue;
                }
                else if (strncmp(arg, "concurrent-jobs", key_len) == 0) {
                    try {
                        worker->d_maxConcurrentJobs = std::stoi(value);
                    }
                    catch (const std::invalid_argument &) {
                        BUILDBOX_LOG_ERROR(
                            "--concurrent-jobs value must be a number: "
                            << argv[0]);
                        return false;
                    }
                }
                else if (strncmp(arg, "stop-after", key_len) == 0) {
                    try {
                        worker->d_stopAfterJobs = std::stoi(value);
                    }
                    catch (const std::invalid_argument &) {
                        BUILDBOX_LOG_ERROR(
                            "--stop-after value must be a number: "
                            << argv[0]);
                        return false;
                    }
                }
                else if (strncmp(arg, "instance", key_len) == 0) {
                    *global_instance_name = std::string(value);
                }
                else if (strncmp(arg, "buildbox-run", key_len) == 0) {
                    worker->d_runnerCommand = value;
                }
                else if (strncmp(arg, "runner-arg", key_len) == 0) {
                    worker->d_extraRunArgs.push_back(value);
                }
                else if (strncmp(arg, "request-timeout", key_len) == 0) {
                    char *end;
                    const long argValue = strtol(value, &end, 10);
                    // assign only upon successful conversion
                    if (*end == 0 && argValue > 0) {
                        worker->d_requestTimeout = argValue;
                    }
                    else {
                        BUILDBOX_LOG_WARNING(
                            "ignoring invalid command-line param \"--"
                            << arg << "=" << value << "\" using default value "
                            << worker->d_requestTimeout);
                    }
                }
                else if (strncmp(arg, "log-level", key_len) == 0) {
                    std::string level(value);
                    std::transform(level.begin(), level.end(), level.begin(),
                                   ::tolower);
                    if (logging::stringToLogLevel.find(level) ==
                        logging::stringToLogLevel.end()) {
                        BUILDBOX_LOG_ERROR("Invalid log level.");
                        return false;
                    }
                    worker->d_logLevel = level;
                    BUILDBOX_LOG_SET_LEVEL(
                        logging::stringToLogLevel.at(level));
                }
                else if (strncmp(arg, "log-file", key_len) == 0) {
                    FILE *fp = fopen(value, "w");
                    if (fp == nullptr) {
                        BUILDBOX_LOG_ERROR("--log-file: unable to write to "
                                           << std::string(value));
                        return false;
                    }
                    fclose(fp);

                    worker->d_logFile = value;
                    BUILDBOX_LOG_SET_FILE(value);
                }
                else if (strncmp(arg, "config-file", key_len) == 0) {
                    worker->d_configFileName = value;
                }
                else {
                    BUILDBOX_LOG_ERROR("Invalid option: " << argv[0]);
                    return false;
                }
            }
            else {
                if (strcmp(arg, "platform") == 0) {
                    // make backward compatible, ie;
                    // --platform OSFamily linux
                    // --platform OSFamily=linux
                    assign = strchr(argv[1], '=');

                    const auto expected_arguments = assign ? 2 : 3;
                    if (argc < expected_arguments) {
                        BUILDBOX_LOG_ERROR("Missing argument option for "
                                           << argv[0]);
                        return false;
                    }

                    if (assign) {
                        const std::string key(
                            argv[1], 0, static_cast<size_t>(assign - argv[1]));
                        const std::string value(assign + 1);
                        worker->d_platform.emplace_back(key, value);
                        ++argv;
                        --argc;
                    }
                    else {
                        BUILDBOX_LOG_WARNING(
                            "deprecated use of the --platform arg: \"--"
                            << arg << " " << argv[1] << " " << argv[2]
                            << "\" detected");
                        worker->d_platform.emplace_back(argv[1], argv[2]);
                        argv += 2;
                        argc -= 2;
                    }
                }
                else if (strcmp(arg, "verbose") == 0) {
                    worker->d_logLevel = "debug";
                    BUILDBOX_LOG_SET_LEVEL(LogLevel::DEBUG);
                }
                else {
                    BUILDBOX_LOG_ERROR("Invalid option: " << argv[0]);
                    return false;
                }
            }
        }
        else if (worker->d_botID.empty()) {
            worker->d_botID = arg;
        }
        else {
            BUILDBOX_LOG_ERROR("Unexpected argument: " << arg);
            return false;
        }
        argv++;
        argc--;
    }

    return true;
}

} // namespace buildboxworker
