/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxworker_cmdlineparser.h>
#include <buildboxworker_config.h>
#include <buildboxworker_worker.h>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommonmetrics_metricsconfigurator.h>
#include <buildboxcommonmetrics_scopedperiodicpublisherdaemon.h>
#include <buildboxcommonmetrics_statsdpublishercreator.h>
#include <cstring>
#include <iostream>
#include <unistd.h>

using namespace buildboxcommon;
using namespace buildboxworker;

namespace {

const int BUILDBOX_WORKER_USAGE_PAD_WIDTH = 32;

std::string generateDefaultBotID()
{
    char hostname[256] = {0};
    gethostname(hostname, sizeof(hostname));
    return std::string(hostname) + "-" + std::to_string(getpid());
}

void usage(const char *name)
{
    std::clog << "usage: " << name << " [OPTIONS] [BOT_ID]\n";
    std::clog << "    --concurrent-jobs=JOBS      Number of jobs to run "
                 "concurrently\n";
    std::clog << "    --stop-after=JOBS           Stop after running this "
                 "many jobs\n";
    std::clog << "    --instance=INSTANCE         Set the instance name of "
                 "both CAS and Bots services \n";
    std::clog << "    --platform KEY=VALUE        Set a platform property\n";
    std::clog << "    --request-timeout=TIME(seconds) Sets the "
                 "timeout for updateBotSession request. Defaults to 1s.\n";
    std::clog
        << "    --buildbox-run=BUILDBOXRUN  Path to buildbox-run binary\n";
    std::clog << "    --runner-arg=ARG            Pass ARG to buildbox-run\n";
    ConnectionOptions::printArgHelp(BUILDBOX_WORKER_USAGE_PAD_WIDTH, "Bots",
                                    "bots-");
    ConnectionOptions::printArgHelp(BUILDBOX_WORKER_USAGE_PAD_WIDTH, "CAS",
                                    "cas-");
    std::clog << "    --log-level=LEVEL           (default: info) Log "
                 "verbosity: "
              << buildboxcommon::logging::stringifyLogLevels() << "\n";
    std::clog << "    --verbose                   Set log level to debug\n";
    std::clog << "    --log-file=FILE             File to write log to\n";
    std::clog << "    --config-file=FILE          Config file\n";
    buildboxcommonmetrics::MetricsConfigurator::usage();
}

} // namespace

int main(int argc, char *argv[])
{
    if (argc == 2 && strcmp(argv[1], "--help") == 0) {
        usage(argv[0]);
        return 0;
    }

    const char *prgname = argv[0];
    argv++;
    argc--;

    // We provide the `--instance` CLI parameter to set an instance name for
    // all services. We'll want to make sure that individual options do not
    // conflict.
    std::string global_instance_name;

    Worker worker;
    buildboxcommonmetrics::MetricsConfigType config;

    const bool parseSuccess =
        Parser::parse(&worker, &config, &global_instance_name, argc, argv);

    if (!parseSuccess) {
        usage(prgname);
        return 1;
    }

    if (worker.d_botID.empty()) {
        worker.d_botID = generateDefaultBotID();
    }

    if (!worker.d_botsServer.d_url) {
        BUILDBOX_LOG_ERROR("Bots server URL is missing");
        return 1;
    }

    if (!worker.d_casServer.d_url) {
        BUILDBOX_LOG_ERROR("CAS server URL is missing");
        return 1;
    }

    if (worker.d_stopAfterJobs > 0 &&
        worker.d_maxConcurrentJobs > worker.d_stopAfterJobs) {
        BUILDBOX_LOG_INFO("WARNING: Max concurrent jobs ("
                          << worker.d_maxConcurrentJobs
                          << ") > number of jobs to stop after ("
                          << worker.d_stopAfterJobs
                          << "). Capping concurrent jobs to "
                          << worker.d_stopAfterJobs << ".");
        worker.d_maxConcurrentJobs = worker.d_stopAfterJobs;
    }

    if (!global_instance_name.empty()) {
        if (worker.d_botsServer.d_instanceName == nullptr &&
            worker.d_casServer.d_instanceName == nullptr) {

            worker.d_botsServer.d_instanceName =
                worker.d_casServer.d_instanceName =
                    global_instance_name.c_str();
        }
        else {
            BUILDBOX_LOG_ERROR(
                "Conflicting instance name parameters. Either use specific "
                "service options or `--instance` to set a single name for "
                "all.");
            return 1;
        }
    }
    else {
        if (worker.d_botsServer.d_instanceName == nullptr) {
            worker.d_botsServer.d_instanceName = "";
        }
        if (worker.d_casServer.d_instanceName == nullptr) {
            worker.d_casServer.d_instanceName = "";
        }
    }

    // set initial state from config file, or default to BotStatus::OK
    // for backwards compatability if none is provided
    worker.d_botStatus =
        Config::getStatusFromConfigFile(worker.d_configFileName);

    // Setup metrics collection
    auto s_publisher =
        buildboxcommonmetrics::StatsdPublisherCreator::createStatsdPublisher(
            config);

    buildboxcommonmetrics::ScopedPeriodicPublisherDaemon<
        buildboxcommonmetrics::StatsDPublisherType>
        statsDPublisherGuard(config.enable(), config.interval(),
                             *s_publisher.get());

    worker.runWorker();
    return 0;
}
