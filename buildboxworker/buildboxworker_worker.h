/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_WORKER
#define INCLUDED_BUILDBOXWORKER_WORKER

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_grpcretry.h>
#include <buildboxcommon_temporaryfile.h>

#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>

#include <condition_variable>
#include <mutex>
#include <set>
#include <string>
#include <vector>

namespace buildboxworker {

using namespace buildboxcommon;

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

class Worker {
  public:
    /**
     * Connect to the Bots and CAS servers and run jobs until
     * `d_stopAfterJobs` reaches 0.
     */
    void runWorker();

    buildboxcommon::ConnectionOptions d_botsServer;
    buildboxcommon::ConnectionOptions d_casServer;

    long d_requestTimeout = 1.0;

    std::shared_ptr<proto::Bots::StubInterface> d_stub;
    std::string d_botID;
    std::vector<std::pair<std::string, std::string>> d_platform;
    std::vector<std::string> d_extraRunArgs;

    int d_maxConcurrentJobs = 1;

    // The worker will stop running after executing this many jobs. If this
    // is negative (the default), it will never stop.
    int d_stopAfterJobs = -1;

    std::string d_runnerCommand = "buildbox-run";

    std::string d_logLevel = "info";
    std::string d_logFile = "";
    std::string d_configFileName = "";

    // The group pids of all spawned subprocesses (set to their pids)
    std::set<pid_t> d_subprocessPgid;
    proto::BotStatus d_botStatus = proto::BotStatus::OK;

    // Decrements the thread counter and issues a notification.
    void decrementThreadCount();

    // Add a subprocess to this worker's tracked subprocesses
    void trackSubprocess(const pid_t subprocessPid);

  protected: // (Allow access to test fixtures)
    // Default time between `UpdateBotSession()` calls:
    static const std::chrono::microseconds s_defaultWaitTime;
    // Max. time between `UpdateBotSession()` calls:
    static const std::chrono::microseconds s_maxWaitTime;

    proto::BotSession d_session;

    // Returns the time to wait for a job. (That time is based on the value of
    // `BotSession::expire_time` and the availability of active jobs.)
    std::chrono::microseconds calculateWaitTime() const;

  private:
    /**
     * Execute the action stored in the given temporary file.
     */
    proto::ActionResult executeAction(TemporaryFile &actionFile);

    void workerThread(const std::string &leaseId);

    std::mutex d_sessionMutex;

    // Notified when a worker thread finishes a job.
    std::condition_variable d_sessionCondition;

    // Total number of detached threads.
    int d_detachedThreadCount = 0;

    // Keep track of Lease IDs that have been assigned to a worker
    // Lease IDs are added when the while-loop spawns worker threads and
    // are removed when by the worker threads when they're done.
    std::set<std::string> d_activeJobs;

    // Keep track of Lease IDs that have been accepted by the worker, but
    // whose acceptance has not yet been acknowledged by the server (so we
    // haven't actually started work on them yet).
    std::set<std::string> d_jobsPendingAck;

    // Register the signal handler. On errors `exit(1)`.
    static void registerSignals();

    // Set the platform properties for `d_session`.
    void setPlatformProperties();

    // Returns whether the main loop in `runWorker()` should keep going.
    bool hasJobsToProcess() const;

    // Functions to process the session's leases.
    void processLeases(bool *skipPollDelay);
    void processPendingLease(proto::Lease *lease, bool *skipPollDelay);
    void processActiveLease(const proto::Lease &lease);
    void processCancelledLease(const proto::Lease &lease);

    // Build the command to send to the runner
    std::vector<std::string>
    buildRunnerCommand(TemporaryFile &actionFile,
                       TemporaryFile &actionResultFile);

    void cleanupAfterUpdateBotSession(grpc::Status updateStatus,
                                      bool shouldExit = false);
};

} // namespace buildboxworker
#endif
