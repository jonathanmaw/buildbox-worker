/*
 * Copyright 2020 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXWORKER_ACTIONUTILS
#define INCLUDED_BUILDBOXWORKER_ACTIONUTILS

#include <buildboxworker_worker.h>

#include <buildboxcommon_temporaryfile.h>

#include <build/bazel/remote/execution/v2/remote_execution.grpc.pb.h>
#include <google/devtools/remoteworkers/v1test2/bots.grpc.pb.h>

namespace buildboxworker {

namespace proto {
using namespace google::devtools::remoteworkers::v1test2;
using namespace build::bazel::remote::execution::v2;
} // namespace proto

struct ActionUtils {
    // Needed for testing
    virtual ~ActionUtils();

    // Read an `ActionResult` proto from the given path.
    static proto::ActionResult readActionResultFile(const char *path);

    // Fork the command and return the child pid
    static pid_t
    runCommandInSubprocess(const std::vector<std::string> &command,
                           Worker *worker);

    /**
     * Execute the action stored in the given temporary file.
     */
    static pid_t
    executeActionInSubprocess(const std::vector<std::string> &command,
                              Worker *worker);

    /*
     * Given a runner subprocess, wait for it to exit
     */
    static void waitForRunner(pid_t subprocessPid, Worker *worker);

    /*
     * Download an action given by the input leaseId to the given
     * TemporaryFile. Returns a bool indicating success.
     */
    static bool downloadActionToFile(
        const std::string &leaseId, TemporaryFile *actionFile,
        const buildboxcommon::ConnectionOptions &casServerConnection,
        proto::BotSession botSession);

    /**
     * Start a subprocess with the given command and return its PID.
     */
    pid_t runWorkerSubprocess(const std::vector<std::string> &command);
};

} // namespace buildboxworker

#endif
